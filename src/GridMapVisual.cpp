/*
 * GridMapVisual.cpp
 *
 *  Created on: Aug 3, 2016
 *  Author: Philipp Krüsi, Péter Fankhauser
 *  Institute: ETH Zurich, ANYbotics
 */

#include <ros/ros.h>
#include <ros/console.h>

#include <rviz/uniform_string_stream.h>
#include <OGRE/OgreMaterialManager.h>
#include <OGRE/OgreTextureManager.h>
#include <OGRE/OgreTechnique.h>

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreManualObject.h>

#include <rviz/ogre_helpers/billboard_line.h>

#include <sensor_msgs/point_cloud2_iterator.h>
#include "tilegrid_rviz_plugin/GridMapVisual.hpp"

namespace tilegrid_rviz_plugin {

GridMapVisual::GridMapVisual(Ogre::SceneManager* sceneManager, Ogre::SceneNode* parentNode)
    : manualObject_(0),
      haveMap_(false)
{
  sceneManager_ = sceneManager;
  frameNode_ = parentNode->createChildSceneNode();

  // Create BillboardLine object.
  meshLines_.reset(new rviz::BillboardLine(sceneManager_, frameNode_));
}

GridMapVisual::~GridMapVisual()
{
  // Destroy the ManualObject.
  sceneManager_->destroyManualObject(manualObject_);
  material_->unload();
  Ogre::MaterialManager::getSingleton().remove(material_->getName());

  // Destroy the frame node.
  sceneManager_->destroySceneNode(frameNode_);
}

void GridMapVisual::setMessage(const tilegrid_msgs::PointCloud2Grid::ConstPtr& msg)
{
  // Convert grid map message.
  //grid_map::GridMapRosConverter::fromMessage(*msg, map_);

  // TODO ensure this isn't leaking mem
  tilepc_msg_ptr_ = msg;
  haveMap_ = true;
}

void GridMapVisual::computeVisualization(float alpha, bool showGridLines, bool flatTerrain, std::string heightLayer,
                                         bool flatColor, bool noColor, Ogre::ColourValue meshColor, bool mapLayerColor,
                                         std::string colorLayer, bool useRainbow, bool invertRainbow,
                                         Ogre::ColourValue minColor, Ogre::ColourValue maxColor,
                                         bool autocomputeIntensity, float minIntensity, float maxIntensity)
{
  if (!haveMap_) {
    ROS_DEBUG("Unable to visualize grid map, no map data. Use setMessage() first!");
    return;
  }

#if 0
  // Get list of layers and check if the requested ones are present.
  std::vector<std::string> layerNames = map_.getLayers();
  if (layerNames.size() < 1) {
    ROS_DEBUG("Unable to visualize grid map, map must contain at least one layer.");
    return;
  }
  if ((!flatTerrain && !map_.exists(heightLayer)) || (!noColor && !flatColor && !map_.exists(colorLayer))) {
    ROS_DEBUG("Unable to visualize grid map, requested layer(s) not available.");
    return;
  }
#endif

#if 0
  const grid_map::Matrix& heightData = map_[flatTerrain ? layerNames[0] : heightLayer];
  const grid_map::Matrix& colorData = map_[flatColor ? layerNames[0] : colorLayer];
#endif

  // TODO
  float resolution = tilepc_msg_ptr_->resolution;
  float min_height = tilepc_msg_ptr_->min_height;

  // initialize ManualObject
  if (!manualObject_) {
    static uint32_t count = 0;
    rviz::UniformStringStream ss;
    ss << "Mesh" << count++;
    manualObject_ = sceneManager_->createManualObject(ss.str());
    frameNode_->attachObject(manualObject_);

    ss << "Material";
    materialName_ = ss.str(); // name is "Mesh0Material"

    material_ = Ogre::MaterialManager::getSingleton().create(materialName_, "rviz");

    material_->setReceiveShadows(false);
    material_->getTechnique(0)->setLightingEnabled(true);
    material_->getTechnique(0)->getPass(0)->setShadingMode(Ogre::SO_FLAT);

    //material_->setAmbient(0.5, 0.5, 0.5);
    //material_->setDiffuse(0.5, 0.5, 0.5);
    //material_->setFog(...)

    // no culling is both sides visible
    //material_->setCullingMode(Ogre::CULL_NONE);
    // clockwise(default), anticlockwise.
    // note ogre expects ANTIclockwise geometry
    material_->setCullingMode(Ogre::CULL_CLOCKWISE);
    // also there's manualCullingMode(MANUAL_CULL_NONE,
    // MANUAL_CULL_BACK(default), MANUAL_CULL_FRONT
  }

  manualObject_->clear();

  size_t n_pts = tilepc_msg_ptr_->pc2.width * tilepc_msg_ptr_->pc2.height;
  if (n_pts == 0) {
    ROS_WARN("empty PointCloud2Grid");
    return;
  }

  // 5*2 triangles per grid cell (5 rectangular faces, two triangles per quad)
  size_t n_vertices = (n_pts*5*2)*3;

  manualObject_->estimateVertexCount(n_vertices);
  manualObject_->estimateIndexCount(n_vertices*3);
  manualObject_->begin(materialName_, Ogre::RenderOperation::OT_TRIANGLE_LIST);

  meshLines_->clear();
  if (showGridLines) {
    meshLines_->setColor(0.2, 0.2, 0.2, alpha);
    meshLines_->setLineWidth(resolution / 10.0);
    meshLines_->setMaxPointsPerLine(2);
    size_t nLines = 8*n_pts;
    meshLines_->setNumLines(nLines);
  }

  using Pc2CItr = sensor_msgs::PointCloud2ConstIterator<float>;

  // Determine max and min intensity.
  if (autocomputeIntensity && !flatColor && !mapLayerColor) {
    Pc2CItr itr_i(tilepc_msg_ptr_->pc2, "intensity");
    for (size_t i=0; i < n_pts; ++i) {
      float intensity = *itr_i;
      minIntensity = std::min(minIntensity, intensity);
      maxIntensity = std::max(maxIntensity, intensity);
      ++itr_i;
    }
  }

  size_t vertex_ctr = 0;

  Pc2CItr itr_xyzi(tilepc_msg_ptr_->pc2, "x");
  for (size_t i=0; i < n_pts; ++i) {
    float x = itr_xyzi[0];
    float y = itr_xyzi[1];
    float z = itr_xyzi[2];
    float intensity = itr_xyzi[3];
    ++itr_xyzi;

    if (z <= min_height) {
      // get a lil' flat box
      z = min_height + 0.01;
    }

    Ogre::Vector3 top_far_left(x - 0.5*resolution, y - 0.5*resolution, z);
    Ogre::Vector3 top_far_right(x + 0.5*resolution, y - 0.5*resolution, z);
    Ogre::Vector3 top_near_left(x - 0.5*resolution, y + 0.5*resolution, z);
    Ogre::Vector3 top_near_right(x + 0.5*resolution, y + 0.5*resolution, z);

    Ogre::Vector3 bottom_far_left(x - 0.5*resolution, y - 0.5*resolution, min_height);
    Ogre::Vector3 bottom_far_right(x + 0.5*resolution, y - 0.5*resolution, min_height);
    Ogre::Vector3 bottom_near_left(x - 0.5*resolution, y + 0.5*resolution, min_height);
    Ogre::Vector3 bottom_near_right(x + 0.5*resolution, y + 0.5*resolution, min_height);

    Ogre::ColourValue lid_color(0.f, 0.f, 0.f, 1.f);
    Ogre::ColourValue lr_color(0.f, 0.f, 0.f, 1.f);
    Ogre::ColourValue nf_color(0.f, 0.f, 0.f, 1.f);

    normalizeIntensity(intensity, minIntensity, maxIntensity);

    if (useRainbow) {
      if (invertRainbow) {
        getRainbowColor(1.0f - intensity, lid_color, lr_color, nf_color);
      } else {
        getRainbowColor(intensity, lid_color, lr_color, nf_color);
      }
    } else {
      getInterpolatedColor(intensity, minColor, maxColor, lid_color, lr_color, nf_color);
    }

#if 0
    // if (flatColor) TODO
    // if (mapLayerColor) TODO
#endif

    //Ogre::ColourValue dbg_color(1.0, 0.0, 0.0, 1.0);

    manualObject_->position(top_far_left); // 0
    manualObject_->colour(lid_color);

    manualObject_->position(top_far_right); // 1
    manualObject_->colour(lid_color);

    manualObject_->position(top_near_left); // 2
    manualObject_->colour(lr_color);

    manualObject_->position(top_near_right); // 3
    manualObject_->colour(lr_color);

    manualObject_->position(bottom_far_left); // 4
    manualObject_->colour(nf_color);

    manualObject_->position(bottom_far_right); // 5
    manualObject_->colour(nf_color);

    manualObject_->position(bottom_near_left); // 6
    manualObject_->colour(nf_color);

    manualObject_->position(bottom_near_right); // 7
    manualObject_->colour(nf_color);

    // lid
    manualObject_->triangle(vertex_ctr + 3, vertex_ctr + 2, vertex_ctr + 1);
    manualObject_->triangle(vertex_ctr + 2, vertex_ctr + 0, vertex_ctr + 1);

    // left
    manualObject_->triangle(vertex_ctr + 6, vertex_ctr + 4, vertex_ctr + 2);
    manualObject_->triangle(vertex_ctr + 4, vertex_ctr + 0, vertex_ctr + 2);

    // right
    manualObject_->triangle(vertex_ctr + 1, vertex_ctr + 5, vertex_ctr + 3);
    manualObject_->triangle(vertex_ctr + 5, vertex_ctr + 7, vertex_ctr + 3);

    // near
    manualObject_->triangle(vertex_ctr + 6, vertex_ctr + 2, vertex_ctr + 7);
    manualObject_->triangle(vertex_ctr + 2, vertex_ctr + 3, vertex_ctr + 7);

    // far
    manualObject_->triangle(vertex_ctr + 5, vertex_ctr + 1, vertex_ctr + 4);
    manualObject_->triangle(vertex_ctr + 1, vertex_ctr + 0, vertex_ctr + 4);

    vertex_ctr += 8;

    // plot grid lines
    if (showGridLines) {
      //lid
      meshLines_->addPoint(top_far_left);
      meshLines_->addPoint(top_far_right);
      meshLines_->newLine();

      meshLines_->addPoint(top_far_right);
      meshLines_->addPoint(top_near_right);
      meshLines_->newLine();

      meshLines_->addPoint(top_near_right);
      meshLines_->addPoint(top_near_left);
      meshLines_->newLine();

      meshLines_->addPoint(top_near_left);
      meshLines_->addPoint(top_far_left);
      meshLines_->newLine();

      // sides
      meshLines_->addPoint(top_far_left);
      meshLines_->addPoint(bottom_far_left);
      meshLines_->newLine();

      meshLines_->addPoint(top_near_left);
      meshLines_->addPoint(bottom_near_left);
      meshLines_->newLine();

      meshLines_->addPoint(top_far_right);
      meshLines_->addPoint(bottom_far_right);
      meshLines_->newLine();

      meshLines_->addPoint(top_near_right);
      meshLines_->addPoint(bottom_near_right);
      meshLines_->newLine();
    }

  } // end for each point

  manualObject_->end();
  material_->getTechnique(0)->setLightingEnabled(false);
  //material_->getTechnique(0)->setLightingEnabled(true);

  if (alpha < 0.9998) {
    material_->getTechnique(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
    material_->getTechnique(0)->setDepthWriteEnabled(false);
  } else {
    material_->getTechnique(0)->setSceneBlending(Ogre::SBT_REPLACE);
    material_->getTechnique(0)->setDepthWriteEnabled(true);
  }
}

void GridMapVisual::setFramePosition(const Ogre::Vector3& position)
{
  frameNode_->setPosition(position);
}

void GridMapVisual::setFrameOrientation(const Ogre::Quaternion& orientation)
{
  frameNode_->setOrientation(orientation);
}

std::vector<std::string> GridMapVisual::getLayerNames()
{
  //TODO
  //return map_.getLayers();
  std::vector<std::string> names;
  return names;
}

// Compute intensity value in the interval [0,1].
void GridMapVisual::normalizeIntensity(float& intensity, float min_intensity, float max_intensity)
{
  intensity = std::min(intensity, max_intensity);
  intensity = std::max(intensity, min_intensity);
  intensity = (intensity - min_intensity) / (max_intensity - min_intensity);
}

// Copied from rviz/src/rviz/default_plugin/point_cloud_transformers.cpp.
void GridMapVisual::getRainbowColor(float intensity,
                                    Ogre::ColourValue& lid_color,
                                    Ogre::ColourValue& lr_color,
                                    Ogre::ColourValue& nf_color)
{
  intensity = std::min(intensity, 1.0f);
  intensity = std::max(intensity, 0.0f);

  float h = intensity * 5.0f + 1.0f;
  int i = floor(h);
  float f = h - i;
  if (!(i & 1)) f = 1 - f;  // if i is even
  float n = 1 - f;

  Ogre::ColourValue color;
  if (i <= 1) color[0] = n, color[1] = 0, color[2] = 1;
  else if (i == 2) color[0] = 0, color[1] = n, color[2] = 1;
  else if (i == 3) color[0] = 0, color[1] = 1, color[2] = n;
  else if (i == 4) color[0] = n, color[1] = 1, color[2] = 0;
  else if (i >= 5) color[0] = 1, color[1] = n, color[2] = 0;

  lid_color = color*0.8;
  lr_color = color*0.6;
  nf_color = color*0.4;
}

// Get interpolated color value.
void GridMapVisual::getInterpolatedColor(float intensity,
                                         Ogre::ColourValue min_color,
                                         Ogre::ColourValue max_color,
                                         Ogre::ColourValue& lid_color,
                                         Ogre::ColourValue& lr_color,
                                         Ogre::ColourValue& nf_color)
{
  intensity = std::min(intensity, 1.0f);
  intensity = std::max(intensity, 0.0f);

#if 0
  Ogre::ColourValue color;
  color.r = intensity * (max_color.r - min_color.r) + min_color.r;
  color.g = intensity * (max_color.g - min_color.g) + min_color.g;
  color.b = intensity * (max_color.b - min_color.b) + min_color.b;
#endif

  // TODO hope this isn't too slow
  float h0, s0, v0;
  min_color.getHSB(&h0, &s0, &v0);
  float h1, s1, v1;
  max_color.getHSB(&h1, &s1, &v1);

  float h = (1-intensity)*h0 + intensity*h1;
  float s = (1-intensity)*s0 + intensity*s1;
  float v = (1-intensity)*v0 + intensity*v1;

  float value_lid = 0.8, value_lr = 0.6, value_nf = 0.4;
  lid_color.setHSB(h, s, v*.5 + value_lid*.5);
  lr_color.setHSB(h, s, v*.5 + value_lr*.5);
  nf_color.setHSB(h, s, v*.5 + value_nf*.5);
}

}  // namespace
