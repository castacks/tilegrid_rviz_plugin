  for (size_t i=0; i < n_pts; ++i) {
    float x = itr_xyz[0];
    float y = itr_xyz[1];
    float z = itr_xyz[2];
    ++itr_xyz;

    if (z <= min_height) {
      // get a lil' flat box
      z = min_height + 0.01;
    }

    Ogre::Vector3 top_far_left(x - 0.5*resolution, y - 0.5*resolution, z);
    Ogre::Vector3 top_far_right(x + 0.5*resolution, y - 0.5*resolution, z);
    Ogre::Vector3 top_near_left(x - 0.5*resolution, y + 0.5*resolution, z);
    Ogre::Vector3 top_near_right(x + 0.5*resolution, y + 0.5*resolution, z);

    Ogre::Vector3 bottom_far_left(x - 0.5*resolution, y - 0.5*resolution, min_height);
    Ogre::Vector3 bottom_far_right(x + 0.5*resolution, y - 0.5*resolution, min_height);
    Ogre::Vector3 bottom_near_left(x - 0.5*resolution, y + 0.5*resolution, min_height);
    Ogre::Vector3 bottom_near_right(x + 0.5*resolution, y + 0.5*resolution, min_height);

    Ogre::Vector3 normal_lid = -(top_near_left - top_far_left).crossProduct(top_far_right - top_far_left);
    normal_lid.normalise();

    Ogre::Vector3 normal_left = -(bottom_far_left - top_far_left).crossProduct(top_near_left - top_far_left);
    normal_left.normalise();

    Ogre::Vector3 normal_right = -normal_left;

    Ogre::Vector3 normal_near = -(bottom_near_left - top_near_left).crossProduct(top_near_right - top_near_left);
    normal_near.normalise();

    Ogre::Vector3 normal_far = -normal_near;

    // lid
    manualObject_->position(top_far_left); manualObject_->normal(normal_lid); manualObject_->colour(face_color3);
    manualObject_->position(top_near_left); manualObject_->normal(normal_lid); manualObject_->colour(face_color3);
    manualObject_->position(top_near_right); manualObject_->normal(normal_lid); manualObject_->colour(face_color3);
    manualObject_->position(top_far_right); manualObject_->normal(normal_lid); manualObject_->colour(face_color3);
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 2, vertex_ctr + 3);
    vertex_ctr += 4;

    // left
    manualObject_->position(top_far_left); manualObject_->normal(normal_left); manualObject_->colour(face_color1);
    manualObject_->position(bottom_far_left); manualObject_->normal(normal_left); manualObject_->colour(face_color1);
    manualObject_->position(bottom_near_left); manualObject_->normal(normal_left); manualObject_->colour(face_color1);
    manualObject_->position(top_near_left); manualObject_->normal(normal_left); manualObject_->colour(face_color1);
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 2, vertex_ctr + 3);
    vertex_ctr += 4;

    // right
    manualObject_->position(bottom_near_right); manualObject_->normal(normal_right); manualObject_->colour(face_color1);
    manualObject_->position(bottom_far_right); manualObject_->normal(normal_right); manualObject_->colour(face_color1);
    manualObject_->position(top_far_right); manualObject_->normal(normal_right); manualObject_->colour(face_color1);
    manualObject_->position(top_near_right); manualObject_->normal(normal_right); manualObject_->colour(face_color1);
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 2, vertex_ctr + 3);
    vertex_ctr += 4;

    // near
    manualObject_->position(top_near_left); manualObject_->normal(normal_near); manualObject_->colour(face_color2);
    manualObject_->position(bottom_near_left); manualObject_->normal(normal_near); manualObject_->colour(face_color2);
    manualObject_->position(bottom_near_right); manualObject_->normal(normal_near); manualObject_->colour(face_color2);
    manualObject_->position(top_near_right); manualObject_->normal(normal_near); manualObject_->colour(face_color2);
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 2, vertex_ctr + 3);
    vertex_ctr += 4;

    // far
    manualObject_->position(top_far_left); manualObject_->normal(normal_far); manualObject_->colour(face_color2);
    manualObject_->position(top_far_right); manualObject_->normal(normal_far); manualObject_->colour(face_color2);
    manualObject_->position(bottom_far_right); manualObject_->normal(normal_far); manualObject_->colour(face_color2);
    manualObject_->position(bottom_far_left); manualObject_->normal(normal_far); manualObject_->colour(face_color2);
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 2, vertex_ctr + 3);
    vertex_ctr += 4;

/// simple box, but everything is per-vertex, not per-face


    manualObject_->position(top_far_left); // 0
    manualObject_->colour(face_color1);

    manualObject_->position(top_far_right); // 1
    manualObject_->colour(face_color1);

    manualObject_->position(top_near_left); // 2
    manualObject_->colour(face_color1);

    manualObject_->position(top_near_right); // 3
    manualObject_->colour(face_color1);

    manualObject_->position(bottom_far_left); // 4
    manualObject_->colour(face_color2);

    manualObject_->position(bottom_far_right); // 5
    manualObject_->colour(face_color3);

    manualObject_->position(bottom_near_left); // 6
    manualObject_->colour(face_color2);

    manualObject_->position(bottom_near_right); // 7
    manualObject_->colour(face_color3);


    // lid
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 2, vertex_ctr + 3, vertex_ctr + 1);

    // left
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 4, vertex_ctr + 6, vertex_ctr + 2);

    // right
    manualObject_->quad(vertex_ctr + 7, vertex_ctr + 5, vertex_ctr + 1, vertex_ctr + 3);

    // near
    manualObject_->quad(vertex_ctr + 2, vertex_ctr + 6, vertex_ctr + 7, vertex_ctr + 3);

    // far
    manualObject_->quad(vertex_ctr + 0, vertex_ctr + 1, vertex_ctr + 5, vertex_ctr + 4);

    vertex_ctr += 8;


